﻿using MofidService;
using NUnit.Framework;

namespace Quiz.Generate
{
    class Test : GenerateTest
    {
        [SetUp]
        public void Setup()
        {
            base.testFunc = (ar) => new Imp().Main(ar);
        }

        [Test]
        public void Run()
        {
            base.Test();
        }
    }
}
