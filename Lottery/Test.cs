﻿using System.Threading.Tasks;
using MofidService;
using NUnit.Framework;

namespace Quiz.Lottery
{
    class Test : LotteryTest
    {
        [SetUp]
        public void Setup()
        {
            base.testFunc = () => new Imp().Main().Result;
        }

        [Test]
        public void Run1()
        {
            base.Test();
        }

        [Test]
        public void Run2()
        {
            base.Test();
        }

        [Test]
        public void Run3()
        {
            base.Test();
        }

        [Test]
        public void Run4()
        {
            base.Test();
        }

        [Test]
        public void Run5()
        {
            base.Test();
        }
    }
}
