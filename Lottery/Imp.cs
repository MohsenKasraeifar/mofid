﻿using MofidService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Lottery
{
    class Imp
    {
        IDataService dataService = new DataService();
        private const int maxThread = 10;

        public async Task<Customer> Main()
        {
            var total = 0;
            var customerPoints = new List<CustomerPoint>();
            while (true)
            {
                var customersAsync = await dataService.GetCustomersAsync();
                var inquiryRequests = customersAsync.Skip(total).Take(maxThread).ToList();
                if (!inquiryRequests.Any())
                {
                    //log
                    break;
                }

                var tasks = Task.Run(() => Parallel.ForEach(inquiryRequests,
                    new ParallelOptions { MaxDegreeOfParallelism = maxThread },
                    customer => { GetCustomerByPoint(customerPoints, customer); }));
                await Task.WhenAll(tasks);
                total += maxThread;
            }

            return WinnerCustomer(customerPoints);
        }

        private Customer WinnerCustomer(List<CustomerPoint> customerPoints)
        {
            var winner = customerPoints.OrderByDescending(a => a.Point).FirstOrDefault();
            return dataService.GetCustomers().FirstOrDefault(a => a.Id == winner.Id);
        }

        private void GetCustomerByPoint(ICollection<CustomerPoint> customerPoints, Customer customer)
        {
            var customerPoint = dataService.GetCustomerPoint(customer.Id);
            customerPoints.Add(new CustomerPoint
                { Id = customer.Id, Name = customer.Name, Point = customerPoint });
        }

        private class CustomerPoint
        {
            public string Name { get; set; }
            public int Point { get; set; }
            public Guid Id { get; set; }
        }
    }
}