﻿using MofidService;
using NUnit.Framework;

namespace Quiz.Nearest
{
    class Test : NearestTest
    {
        [SetUp]
        public void Setup()
        {
            base.testFunc = (c) => new Imp().Main(c);
        }

        [Test]
        public void Run()
        {
            base.Test();
        }
    }
}
